#!/usr/bin/python3
# -*- mode: python -*-

#   Copyright (c) 2000       Jason Gunthorpe <jgg@debian.org>
#   Copyright (c) 2001-2003  James Troup <troup@debian.org>
#   Copyright (c) 2004       Joey Schulze <joey@debian.org>
#   Copyright (c) 2008       Peter Palfrader <peter@palfrader.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

from __future__ import print_function

import getopt
import os
import pwd
import sys

import ldap

from six.moves import input

from userdir_ldap.ldap import passwdAccessLDAP, BaseDn, GroupObjectClasses, GetAttr
import userdir_ldap.gpg as userdir_gpg


# This tries to search for a free UID. There are two possible ways to do
# this, one is to fetch all the entires and pick the highest, the other
# is to randomly guess uids until one is free. This uses the former.
# Regrettably ldap doesn't have an integer attribute comparision function
# so we can only cut the search down slightly

# [JT] This is broken with Woody LDAP and the Schema; for now just
#      search through all GIDs.
def GetFreeID(lc):
   Attrs = lc.search_s(BaseDn, ldap.SCOPE_ONELEVEL,
                       "gidNumber=*", ["gidNumber"])
   HighestUID = 0
   for group in Attrs:
      ID = int(GetAttr(group, "gidNumber", "0"))
      if ID > HighestUID and ID < 60000:
         HighestUID = ID
   return HighestUID + 1


# Main starts here
AdminUser = pwd.getpwuid(os.getuid())[0]

# Process options
ForceMail = 0
OldGPGKeyRings = userdir_gpg.GPGKeyRings
userdir_gpg.GPGKeyRings = []
(options, arguments) = getopt.getopt(sys.argv[1:], "u:")
for (switch, val) in options:
   if (switch == '-u'):
      AdminUser = val

lc = passwdAccessLDAP(BaseDn, AdminUser)

while 1:
   Group = input("Group name? ")
   if Group == "":
      sys.exit(1)

   Attrs = lc.search_s(BaseDn, ldap.SCOPE_ONELEVEL, "gid=" + Group)
   if len(Attrs) == 0:
      break
   print("Group already exists")

Id = GetFreeID(lc)
print("Create group %s ID = %d" % (Group, Id))

# Submit the add request
Dn = "gid=" + Group + "," + BaseDn
print("Updating LDAP directory..", end="")
sys.stdout.flush()
lc.add_s(Dn, [("gid", Group.encode('utf-8')),
              ("gidNumber", str(Id).encode('ascii')),
              ("objectClass", [oc.encode('ascii') for oc in GroupObjectClasses])])
print()
